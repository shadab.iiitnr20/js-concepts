// --------------------
// ways of making an onject

// // using create method
// var object1 = Object.create(Object());
// object1.name = "Shadab";
// object1.age = 24;
// console.log(object1);

// // Using function cunstructor
// function myInfo(name, age, gender){
//     this.name = name;
//     this.age = age;
//     this.gender = gender;
// }

// var object2 = new myInfo("Shadab", 24, "male");
// var object3 = new myInfo("Salman", 30, "male");
// console.log(object2, object3);

// //using assign method
// var object4 = Object.assign({} , object2);
// console.log(object4);

// //using cunstructor object
// var person = new Object();
// person.name = "Aamir";
// person.age = 26;
// console.log(person);
// console.log(typeof(person));

// // literal syntax
// var object5 = {
//     name: "Ranbir",
//     age: 25,
//     job: "actor",
//     info: function() {
//         return this.job;
//     }
// }
// console.log(object5);
// console.log(object5.info(), object5.name);

// // ES6 class synatx
// class Person2 {
//     constructor (name) {
//         this.name = name;
//     }
// }

// var object6 = new Person2("Shahrukh");
// console.log(object6);

// function particle(x, y) {
//     this.x = x;
//     this.y = y;
//     this.show = function() {

//     }
// };

// particle.prototype.show = function(){

// };

// var p1 = new particle(1,2);
// var p2 = new particle(3,4);
// console.log(p1);
// console.log(p2);

// let F = function () {
//     this.a = 1;
//     this.b = 2;
// };
// let o = new F();
// F.prototype.c = 3;
// F.prototype.b = 4;
// console.log(o);
// console.log(o.a);
// console.log(o.b);
// console.log(o.b);
// console.log(o.c);
// console.log(o.d);

// console.log("Start");

// function loginUser(email, password, callback) {
//     setTimeout(() => {
//         console.log("Test Print");
//         callback ({emailid : email});
//     },2000);
// }

// function getVideo(email, callback) {
//     setTimeout(() => {
//         callback (['video1', 'video2', 'video3']);
//     },2000)
// }

// loginUser("abcd@acbd", 3682, user => {
//     console.log(user);
//     getVideo(user.emailid, video => {
//         console.log(video);
//     })
// });

// // console.log(User);

// // setTimeout(()=>{
// //     console.log("in Between")
// // },1000);

// // console.log("end");

// const object1 = new Object();
// console.log(typeof(object1));
// console.log("Break");

// const object2 = Object.create(Object());
// console.log(typeof(object2));

// function myInfo(name, age) {
//     this.name = name;
//     this.age = age;
// };

// const object3 = new myInfo("Shadab", 24);
// console.log(object3);

// const object4 = {
//     name: "Aamir",
//     age: 30,
//     myDetails: function() {
//         return this.age;
//     }
// };

// console.log(object4.myDetails());

// class Person {
//     constructor (name, age) {
//         this.name = name;
//         this.age = age;
//     }
// }

// const object5 = new Person("Shahrukh", 35);
// console.log(object5);

// const object6 = Object.assign({}, object5);
// console.log(object6)

////////// 31 january 2022

// const obj1 = {              //literal syntax
//     name: "Shadab",
//     age: 24,
//     myInfo: function() {
//         console.log(this.name + " of age " + this.age);
//     }
// }

// //console.log(obj1);
// //obj1.myInfo();

// function myDetails(name, age, job) {   ///functional cunstructor
//     this.name = name;
//     this.age = age;
//     this.job = job;
// }

// const obj2 = new myDetails("Shadab", 24, "Dev");
// const obj3 = new myDetails("Aamir", 30, "QA");
// //console.log(obj2);
// //console.log(obj3);

// const obj4 = new Object();   //object cunstructor
// console.log(obj4);
// obj4.name = "Shadab";
// obj4.age = 24;
// //console.log(obj4);

// const obj5 = Object.create(obj3);  //create method -- passes the prototype object as parameter
// console.log(obj5);

// const obj6 = Object.assign({}, obj4);  // assign method
// console.log(obj6);
// obj6.name = "Yumna";
// console.log(obj6);
// console.log(obj4);

// class Person {   //ES6 class syntax
//     constructor (name, age) {
//         this.name = name;
//         this.age = age;
//     }
// }

// const obj7 = new Person("Shadab", 24);
// console.log(obj7);

// Call Apply Bind

// let obj1 = {
//     name: "Shadab",
//     age: 24
// }

// let obj2 = {
//     name: "Kohli",
//     age: 33
// }

// function getInfo(city, state) {
//     console.log(this.name + " is of age " + this.age + " from " + city + " " + state);
// }

// //function borrowing
// getInfo.call(obj1, "korba", "CG");
// getInfo.call(obj2, "mumbai", "MH");
// getInfo.apply(obj1, ["korba", "CG"]);

// let myInfo = getInfo.bind(obj2, "mumbai", "MH");
// console.log(myInfo);
// myInfo();
// console.log(obj2);

// JSON Parse and JSON Stringfy

// let string1 = '{"name": "shadab" , "age": 24}';
// let obj1 = JSON.parse(string1);
// console.log(obj1);

// obj2 = {
//     name: "Aamir",
//     age: 30
// }

// string2 = JSON.stringify(obj2);
// console.log(string2);
// console.log(typeof(string2));

// // Array Slice
// let arr1 = [1,2,3,4,"Shadab",true,2.44];
// // console.log(arr1[0]);
// // console.log(arr1[4]);
// // console.log(arr1[5]);
// console.log(arr1.slice(0));
// console.log(arr1.slice(0,3));
// console.log(arr1.slice(4));
// // console.log(arr1);

// let arr2 = [1,2,3,4,5,6,7,8];
// //console.log(arr2.splice(0));
// console.log(arr2.splice(3,2));
// console.log(arr2);

// const myMap = new Map();

// const key1 = "string",
//   key2 = {},
//   key3 = function () {};

// myMap.set(key1, "This is a string");
// myMap.set(key2, "This is an empty obj");
// myMap.set(key3, 3);

// //console.log(myMap);

// const value1 = myMap.get(key3);
// //console.log(value1);

// for (let [key, value] of myMap) {
//   console.log(key, value);
// }

// arr = [1,2,3,4,5,6,7,8,9];
// // console.log(arr.slice(0)); //1,2,3,4,5
// // console.log(arr.slice(4)); //5
// // console.log(arr.slice(2,4)); //3,4

// arr2 = arr.splice(3,3,465,567);
// console.log(arr2);
// console.log(arr);

// const myMap = new Map();

// const key1 = "String", key2 = {}, key3 = function(){}

// myMap.set(key1, "This is a string");
// myMap.set(key2, "This is an object");
// myMap.set(key3, "This is a funvtion");
// console.log(myMap);
// console.log(myMap.size);

// const value1 = myMap.get(key2);
// console.log(value1);

// for (let [key, value] of myMap){
//     console.log(key, value);
// }

// let obj1 = {
//     name: "Shadab",
//     age: 24,
//     job: "dev"
// }

// console.log(obj1.size);

// document.querySelector("#parent").addEventListener("click", (event) => {
//   console.log(event.target.id);
//   window.location.href = '/' + event.target.id;
// });

// const list = [
//   { name: "Eggs", price: 20 },
//   { name: "Apple", price: 30 },
//   { name: "Milk", price: 50 },
//   { name: "Bhindi", price: 10 },
//   { name: "Oil", price: 100 },
// ];

// // console.log(list);

// list.forEach((item) => {
//   //console.log(`the price of ${item.name} is ${item.price}`);
// });
// //console.log(list);

// const arr1 = [1, 2, 3, 4, 5, 6];
// console.log(arr1);
// arr2 = arr1.forEach((item) => {
//   return item * 2;
// });
// //console.log(arr2);

// arr3 = arr1.map((item) => {
//   return item * 2;
// });
// //console.log(arr3);

// arr4 = arr1.filter((item) => {
//   return item%2 == 0;
// })
// //console.log(arr4);

// arr5 = arr1.find((item) => {
//   return item%2 === 0;
// })
// //console.log(arr5);

// const arr6 = [23,12,56,35,90,0];
// const arr7 = arr6.sort((a,b) => a-b);
// console.log(arr7);

// const arr1 = [11, 12, 13, 14, 15];

//console.log(arr1.slice());
// const arr2 = arr1.every((item) => {

//   return item>14;
// });

// console.log(arr2);

// const arr2 = arr1.reduce((prevValue, item) => {
//   console.log("----------");
//   console.log(prevValue);
//   console.log(item);
//   return prevValue + item;
// },10);
// console.log(arr2);

// console.log(arr1.splice());
// console.log(arr1);

// const obj = {
//   name: "Shadab",
//   age: 24,
//   isProgrammer: true,
//   address: "Bilaspur"
// }

// //console.log(JSON.stringify(obj));

// const str = '{"name":"shadab", "age":24, "isHome": true}';
// console.log(JSON.parse(str));

// var x = 7;

// function getName () {
//   console.log("My Name is Shadab");
// }

// console.log(x);
// getName();
// console.log(getName);

// const radius = [1, 2, 3, 4];

// function getArea(radius) {
//   return Math.PI * radius * radius;
// }

// function calculate(radius, logic) {
//   output = [];
//   for (i = 0; i < radius.length; i++) {
//     output.push(logic(radius[i]));
//   }
//   return output;
// }

// console.log(calculate(radius, getArea));

// const arr = [1, 2, 3, 4];
// function double(x) {
//   return x * 2;
// }
// console.log(arr.map(double));

// const obj = {
//     name:"Shadab",
//     getName: function() {
//         console.log(this);
//         console.log(this.name);
//         setTimeout(function(){
//             console.log(this);
//             console.log(this.name);
//         },1000)
//         }
//     }

// obj.getName();

// function fname1() {

// }

// console.log(fname1.prototype);
// console.log(new fname1());

// const fname2 = () => {

// }

// console.log(fname2.prototype);
// console.log(new fname2());

// function a() {
//     console.log(b);
// }
// const b = 10;
// a();

// {
//     var a =10;
//     let b=20;
//     const c =30;
//     console.log(a);
//     console.log(b);
//     console.log(c);

// }

// console.log(a);
// console.log(b);
// console.log(c);

// const obj = {
//     name: "Shadab",
//     age: 24,
//     getInfo: function() {
//         console.log(this);
//         console.log(this.name);
//         setTimeout(() => {
//             console.log(this);
//             console.log(this.name);
//         },2000)
//     }
// }

// obj.getInfo();

// function x() {
//   var a = 10;
//   function y() {
//     console.log(a);
//   }
//   return y;
// }

// const z = x();

// z();

// function x() {
//   for (var i = 1; i <= 5; i++) {
//     function close(i) {
//       setTimeout(function () {
//         console.log(i);
//       }, i * 1000);
//     }
//     close(i);
//   }
// }

// x();

// function x() {
//   const a =10;
//   function y() {
//     console.log(a);
//   }
//   return y;
// }

// const z = x();
// z();const counter

// function counter() {
//   let count = 0;
//   this.increment = function() {
//     count++
//     console.log(count)
//   }
//   this.decrement = function() {
//     count--
//     console.log(count)
//   }
// }

// const counter1 = new counter();
// counter1.increment();

// console.log(2+'2');
// console.log(2-'2');

// let x = 2+'2';
// console.log(x);
// console.log(typeof(x));

// y = x + "abc";
// console.log(y);
// console.log(typeof(y));

// let arr = [1,2,2,3];

// console.log([...new Set(arr)]);

// function a() {

//   (function(){
//     let l = 20;
//     var v = 30;
//   })();

//   console.log(v);
//   console.log(l);

// }

// a();

// const obj = {
//   x: "name",
//   y: 3,
//   age:22
// }

// console.log(obj)
// Object.seal(obj);
// obj.y = 4;
// delete obj.x;
// obj.z = 10;
// console.log(obj)

// var array1 = new Array(3);
// console.log(array1);

// var array2 = [];
// array2[2] = 100;
// console.log(array2);

// var array3 = [,,,];
// console.log(array3);

// var myChars = ['a', 'b', 'c', 'd']
// delete myChars[0];
// console.log(myChars);
// console.log(myChars[0]);
// console.log(myChars.length);

// let num = 4321;
// let arr = [];
// while(num!=0){
//   num=num/10;
//   Math.floor(num);
//   console.log(num);
// }

// const obj1 = {
//   name : "Shada",
//   myInfo: function() {
//     console.log(this);
//     console.log(this.name);

//     function myInfo2 (){
//       console.log(this)
//       console.log(this.name);
//     }

//     myInfo2();
//   }
// }

// obj1.myInfo()
// console.log(this.name)
// console.log(window)
// this.name = "Shadab"

// console.log(this.name);

// let myName = () => {
//   console.log(name);
// }

// myName();

// const func = () => {

// }

// console.log(func.prototype);

// function func2() {

// }

// console.log(func2.prototype)

// const obj1 = {
//   name: "Shadab",
//   age: 24
// }

// function myInfo(city, state, country) {
//   console.log(`my name is ${this.name} of ${this.age} from ${city}, ${state}, ${country}`)
// }

// Function.prototype.mybind = function(...args) {
//   const fn = this;
//   params = args.slice(1);
//   return function(...args2) {
//     fn.apply(args[0], [...params,...args2])
//   }
// }

// myInfo.call(obj1, "Bsp")
// myInfo.call(obj1, ["Blr"])
// myInfo.bind(obj1, "Hyd", "TL")("India")
// myInfo.mybind(obj1, "Delhi", "DL")("India")

// // getMessage();

// // var getMessage = () => {
// //      console.log("Good morning");
// // };

// let array = [1,2,3,[4,5,[6,7,8],9]];
// console.log(array.flat(Infinity));
// const flatten = (arr) => {
//   newArray = arr.reduce((acc, item) => {
//     if(Array.isArray(item)){
//       acc = acc.concat(flatten(item))
//     }
//     else{
//       acc.push(item)
//     }
//     return acc
//   },[])
//   return newArray
// }

// console.log(flatten(array))

// let str = "My name is Shadab";

// const reverseStr = (str) => {
//   return str.split("").reduce((acc,item)=>item+acc," ")
// }

// console.log(reverseStr(str));

// function abc() {
//   console.log(a);

//   var a = 10;
// }

// abc();

// const add = (a) => (b) => b ? add(a + b) : a;
// console.log(add(5)(2)(4)(5)());

// const calc = {
//   total: 0,
//   add: function (a) {
//     this.total += a;
//     return this;
//   },
//   multiply: function (a) {
//     this.total *= a;
//     return this;
//   },
//   subtract: function (a) {
//     this.total -= a;
//     return this;
//   },
// };

// const result = calc.add(10).multiply(5).subtract(30).add(10);
// console.log(result.total);

// let nums = [1,2,3,4];
// const mapArray = nums.map((num, i, arr)=>{
//   return num*3;
// })

// console.log(mapArray);

// Array.prototype.myMap = function(cb) {
//   let temp = [];
//   for (let i = 0; i<this.length; i++) {
//     temp.push(cb(this[i],i,this));
//   }
//   return temp;
// }
// const mapArray2 = nums.myMap((num, i, arr)=>{
//   return num*3;
// })

// console.log(mapArray2);

// let num = [1,2,3,4];
// const fltr = num.filter((num,i,arr)=>{
//   return num>2;
// })
// console.log(fltr);

// Array.prototype.myFilter = function(cb) {
//   let temp = [];
//   for(let i=0;i<this.length;i++){
//     if(cb(this[i],i,this)) temp.push(this[i]);
//   }
//   return temp;
// }

// const fltr2 = num.myFilter((num,i,arr)=>{
//   return num>2;
// })
// console.log(fltr2);

// Array.prototype.myReduce = function(cb, initialValue) {
//   let acc = initialValue;
//   for(let i = 0; i< this.length ; i++) {
//     acc = acc ? cb(acc, this[i], i, this) : this[i]
//   }
//   return acc;
// }

// const sum = num.myReduce((acc, item)=>{
//   return acc+item;
// },0)

// console.log(sum);

// const arr = [[1,2,3,4,5,6]];
// console.log(arr.flat());

// const obj1 = {
//   22: 1,
//   b: 2,
//   c: 3,
//   d: 4,
// }

// console.log(Object.keys(obj1));
// console.log(Object.entries(obj1).flat());
// console.log(Object.values(obj1));

// for (let i in obj1) {
//   console.log(i)
// }
// for (let i of obj1) {
//   console.log(i)
// }

// const arr = [11,22,33,44,55,66];
// str = "Winter is here"
// for (let i in str) {
//   console.log(i)
// }

// for (let i of str) {
//   console.log([i])
// }

// const str = "aaabbcccccd";
// for (let i of str) {
//   console.log(i);
// }

// let obj = {}
// for (let i of str) {
//   if(obj[i]) {
//     obj[i]++;
//   }
//   else {
//     obj[i] = 1;
//   }
//   for(let i in obj) {
//     if(obj[i]===1){

//     }
//   }

// }
// console.log(JSON.stringify(obj));

// const arr = [1, 2, [3, 4, [5, 6], 7], 8, 9];

// console.log(arr.flat(Infinity));

// const flatArray = (arr) => {
//   newArray = arr.reduce((acc, item) => {
//     if (Array.isArray(item)) {
//       acc = acc.concat(flatArray(item));
//     } else {
//       acc.push(item);
//     }
//     return acc;
//   }, []);
//   return newArray;
// };

// console.log(flatArray(arr));

// const str1 = "Winter is here";
// const str2 = "Shadab";

// const reverseStr = (str) => {
//   return [...str].reduce((acc, item) => item + acc, "");
// };

// const reverseStr2 = (str) => {
//   return str.split(" ").reverse().join(" ");
// };

// console.log(reverseStr(str1));
// console.log(reverseStr(str2));
// console.log(reverseStr2(str1));
// //console.log(reverseStr2(str2));

// // const obj1 = {
// //   name:"Shadab",
// //   age: 24
// // }

// // function myInfo (city,country){
// //   //console.log(this.name)
// //   console.log(`My name is ${this.name} of ${this.age}, from ${city}, ${country}`)
// // }

// // myInfo.bind(obj1,"Bsp")("India")

// // Function.prototype.myBind = function(...args) {
// //   const fn = this
// //   let params = args.slice(1)
// //   return function(...args2){
// //     fn.apply(args[0], [...params,...args2])
// //   }
// // }

// // myInfo.myBind(obj1,"Bsp")("India")

// let str = "my_var_func";
// const q1 = (str) => {
//   temp = [...str];
//   //console.log(temp)
//   for (let i = 0; i < temp.length; i++) {
//     if (temp[i] === "_") {
//       temp.splice(i, 1);
//       temp[i] = temp[i].toUpperCase();
//     }
//   }
//   return temp.join("");
// };

// console.log(q1(str));
// console.log(q1("_my_var"));

// const myArr = [1,2,3,4,5,6];
// const mul = myArr.map((x)=>x*2);
// console.log(mul)

// Array.prototype.myMap = function(cb) {
//   let temp = []
//   for(let i = 0; i<this.length; i++) {
//     temp.push(cb(this[i],i,this))
//   }
//   return temp
// }

// Array.prototype.myFilter = function(cb) {
//   let temp = [];
//   for(let i = 0; i < this.length; i++) {
//     if(cb(this[i],i,this)) temp.push(this[i]);
//   }
//   return temp;
// }

// const mul1 = myArr.myFilter((x)=>x>3);
// console.log(mul1)

// function newObj(str){
//   let obj = {};
//   for(let item of str) {
//     if(obj[item]){
//       obj[item]++;
//     }
//     else{
//       obj[item] = 1;
//     }
//   }
//   return obj;
// }

// console.log(newObj("aaabbcccccdd"))

// const newStr = "Shadab";
// console.log(newStr.slice(1,4))

// console.log("Person1 : Tickets")
// console.log("Person2 : Tickets")

// const getTickets = new Promise((res,rej)=> {
//   setTimeout(()=>{
//     res("Got the tickets");
//   },3000);
// })

// const getPopcorn = getTickets.then((value)=>{
//   console.log("Husband we should go in");
//   console.log("Wife No! I am hungry");
//   return new Promise((res,rej)=>res(`${value}, popcorn`))
// })

// const getSoda = getPopcorn.then((value)=> {
//   console.log("Husband gets popcorn")
//   console.log("Husband should we now go in!!");
//   console.log("Wife: No! I need soda to!!!!");
//   return new Promise((res,rej)=> res(`${value} and Soda`))
// });

// getSoda.then((value)=> console.log(value));

// console.log("Person4 : Tickets")
// console.log("Person5 : Tickets")

// async function func() {
//   await 10;
// }
// console.log(func());

// async function func() {
//   return 10;
// }
// console.log(func());

// const str = "aabbbccccd";

// const output = (str) => {
//   const obj = {}
//   for (let i of str) {
//     if(obj[i]) {
//       obj[i]++;
//     }else {
//       obj[i] = 1;
//     }
//   }
//   console.log(obj)
//   for (let item in obj) {
//     if(obj[item]===1){
//       //return item;
//       return str.indexOf(item);
//     }
//   }
// }

// console.log(output(str));

// const arr = [1,2,3,4,5];
// for (let i of arr) {
//   console.log(i);
// }
// for (let i in arr) {
//   console.log(i);
// }

// var a = 10;
// let b = 20;
// const c = 30;

// console.log(a,b,c);

// function test(){
//   console.log(a,b,c);
// }

// test();
// console.log(a,b);
// var a = 10;
// let b = 20;

// {
//   console.log(a,b);
//   let a = 100;
//   var b = 200;
//   console.log(a,b)
// }

// function square(num) {
//   return num * num;
// }

// function displaySquare(fn) {
//   console.log("The square is " + fn(5));
// }

// displaySquare(square);

// for (let i = 0; i < 5; i++) {
//   setTimeout(() => {
//     console.log(i);
//   }, i * 1000);
// }

// (function sqr(num){
//   console.log(num*num);
// })(5);

// {
//   let a = 5;
// }
// console.log(a);

// let a;
// {
//   let a;
// }

// const obj1 = {
//   name: "Shadab",
//   age: 24,
// };

// function myInfo(city, country) {
//   console.log(
//     `My name is ${this.name} of age ${this.age} of ${city} , ${country}`
//   );
// }

// myInfo.call(obj1, "bsp", "india");
// myInfo.apply(obj1, ["Bsp", "India"]);

// Function.prototype.myBind = function (...args) {
//   const fn = this;
//   let params = args.slice(1);
//   return function (...args2) {
//     fn.apply(args[0], [...params, ...args2]);
//   };
// };
// myInfo.myBind(obj1)("Hyd", "India");

// Function.prototype.mybind = function(...args) {
//   const fn = this;
//   params = args.slice(1);
//   return function(...args2) {
//     fn.apply(args[0], [...params,...args2])
//   }
// }

// let arr = [1, 2, 3, 4];
// console.log(arr.slice(1));

// const temp = arr.reduce((acc, cur) => {
//   return acc + cur;
// }, 0);
// console.log(temp);

// Array.prototype.myMap = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     temp.push(cb(this[i]));
//   }
//   return temp;
// };

// const arr3 = [1, 2, 3, 4, 5];

// const newMap = arr3.myMap((x) => {
//   return x * 5;
// });

// console.log(newMap);

// Array.prototype.myFilter = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     if (cb(this[i])) temp.push(this[i]);
//   }
//   return temp;
// };

// const newFlt = arr3.myFilter((x) => {
//   return x > 3;
// });

// console.log(newFlt);

// Array.prototype.myReduce = function (cb, initialValue) {
//   acc = initialValue;
//   for (let i = 0; i < this.length; i++) {
//     acc = acc ? cb(acc, this[i], i, this) : this[i];
//   }
//   return acc;
// };

// const newArr = [1, 2, 3, 4];
// const newSum = newArr.myReduce((acc, cur) => {
//   return acc + cur;
// });
// console.log(newSum);

// let students = [
//   { name: "Shadab", age: 31, marks: 80 },
//   { name: "Shahrukh", age: 15, marks: 69 },
//   { name: "Salman", age: 16, marks: 35 },
//   { name: "Aamir", age: 7, marks: 55 },
// ];

// const ans = students.map((student) => {
//   // console.log(student);
//   return student.name.toUpperCase();
// });
// console.log(ans);

// const marks = students
//   .filter((student) => {
//     return student.marks > 60;
//   })
//   .map((student) => {
//     return student.name;
//   });
// console.log(marks);

// const sumMarks = students.reduce((acc, cur) => {
//   return acc + cur.marks;
// }, 0);
// console.log(sumMarks);

// const totalMarks = students
//   .map((student) => {
//     if (student.marks < 60) {
//       student.marks += 20;
//     }
//     return student;
//   })
//   .filter((student) => {
//     return student.marks > 60;
//   })
//   .reduce((acc, cur) => {
//     return acc + cur.marks;
//   }, 0);

// console.log(totalMarks);

// (function square(num) {
//   console.log(num * num);
// })(5);

// // function displaySquare(fn){
// //   console.log("The Square is " + fn)
// // }

// // displaySquare(square(5));

// // for(let i =0; i<5; i++){
// //   setTimeout(()=>{
// //     console.log(i);
// //   },i*1000)
// // }

// const obj = {
//   name: "Shadab",
//   age: 24,
// };

// function myInfo(city, country) {
//   console.log(
//     `my name is ${this.name} and age is ${this.age} from ${city} ${country}`
//   );
// }

// const fn1 = myInfo.bind(obj);
// fn1("hyd", "ind");

// Function.prototype.myBind = function (...args) {
//   const fn = this;
//   let params = args.slice(1);
//   return function (...args2) {
//     fn.apply(args[0], [...params, ...args2]);
//   };
// };

// const fn2 = myInfo.myBind(obj, "HYD");
// fn2("IND");

// //polyfill of map,filter & reduce

// Array.prototype.myMap = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     temp.push(cb(this[i], i, this));
//   }
//   return temp;
// };

// let arr = [1, 2, 3, 4];
// const newArr = arr.myMap((x) => {
//   return x * 2;
// });

// console.log(newArr);

// Array.prototype.myFilter = function(cb) {
//   let temp = [];
//   for(let i=0;i<this.length;i++){
//     if(cb(this[i])) temp.push(this[i])
//   }
//   return temp;
// }

// const newArr1 = arr.myFilter((x)=>{
//   return x > 2;
// })

// console.log(newArr1);

// Array.prototype.myReduce = function(cb,initialValue) {
//   let acc = initialValue;
//   for(let i = 0; i < this.length; i++){
//     acc = acc ? cb(acc,this[i],i,this) : this[i];
//   }
//   return acc;
// }

// const sum1 = arr.myReduce((acc,cur)=>{
//   return acc+cur;
// },0)
// console.log(sum1);
// let a = 10;
// const b = 20;
// var c = 30;

// function varrr () {
//   let a = 100;
//   const b = 200;
//   var c = 300;
//   console.log(a,b,c);
// }

// varrr();

// function func1 () {
//   userName = "Shadab";
//   function func2 () {
//     console.log(userName);
//   }
//   return func2;
// }

// const newFunc = func1();
// newFunc();

// function createBase(num){
//   return function(num2){
//     return num+num2;
//   }
// }

// var addSix = createBase(6);
// console.log(addSix(10));//16
// console.log(addSix(21));//27

// function find() {
//   let a = [];
//   for (let i = 0; i < 100000; i++) {
//     a[i] = i * i;
//   }
//   return function (index) {
//     console.log(a[index]);
//   };
// }

// const closure = find();
// console.time("6");
// closure(6);
// console.timeEnd("6");

// function a() {
//   for (var i = 0; i < 3; i++) {
//     function b(i) {
//       setTimeout(() => {
//         console.log(i);
//       }, i * 1000);
//     }
//     b(i);
//   }
// }
// a();

// function counter() {
//   _counter = 0;

//   function add(num) {
//     _counter = _counter + num;
//   }

//   function retrieve() {
//     return "Counter is " + _counter;
//   }

//   return {
//     add,
//     retrieve,
//   };
// }

// const c = counter();
// c.add(5);
// console.log(c.retrieve());
// c.add(10);
// console.log(c.retrieve());

//sum(1)(2)(3);

//function sum1(a) {
//   return function (b) {
//     return function (c) {
//       console.log(a + b + c);
//     };
//   };
// }

// sum1(2)(6)(1);

//const sum = (a) => (b) => b ? sum(a + b) : a;

// console.log(sum(1)(2)(3)());

// function operation(opr) {
//   return function (num1) {
//     return function (num2) {
//       if (opr === "sum") {
//         return num1 + num2;
//       } else if (opr === "mul") {
//         return num1 * num2;
//       } else if (opr === "div") {
//         return num1 / num2;
//       } else if (opr === "sub") {
//         return num1 - num2;
//       }
//     };
//   };
// }
// //console.log(operation("sub")(4)(2));

// function DOM(id){
//   return function(content){
//      document.getElementById(id).textContent = content;
//   }
// }

// const updateContent = DOM("heading");

// updateContent("Shadab");
// updateContent("Hero");
// updateContent("Iron Man");

// function curry(func) {
//   return function curried(...args){
//     if(args.length >= func.length){
//       return func(...args);
//     } else {
//       return function(...next) {
//         return curried(...args, ...next);
//       }
//     }
//   }
// }

// const sum = (a,b,c,d) => a+b+c+d;

// const totalSum = curry(sum);
// console.log(totalSum(1)(6)(5)(6));

// console.log("a"+"b");

// console.log()

// let num = 57745;
// // console.log(num.split());

// let a = num.toString().split('');

// console.log(Math.min(...a));

// --------------------------------- 22nd May 3 Questions ----------------------------//
// const num = 1234;

// let a = num.toString().split(''); ['1','2','3','4']
// let b = Math.max(...a);
// console.log(typeof(b));

// const digitize = (n) => {
//   return [...`${n}`].map((i)=>parseInt(i)) [1,2,3,4]
// }

// console.log(digitize(num));

// const ogArr = (arr,args) => {
//   console.log(args);
//   console.log(arr);
//   //let argState = Array.isArray(args[0])? args[0] : args;
//   const pulled = arr.filter((i) => !args.includes(i));
//   return pulled;
//   //console.log(argState);
// }

// const arr = [1,'a',1,3,4,5,6];
// console.log(ogArr(arr,[1,'a']));

// console.log("-----------------------------------------")

// let arr1 = ["a", "b", "c", "d", "e", "f"];

// const ogfltr = (arr, pullarr) => {
//   // console.log(pullarr);
//   // console.log(arr);
//   let remove = [];
//   const pulled = arr
//     .map((ele, i) => (pullarr.includes(i) ? remove.push(ele) : ele))
//     .filter((ele, i) => !pullarr.includes(i));
//   return pulled;
//   // console.log(remove);
// };

// console.log(ogfltr(arr1, [1, 3]));

// console.log("-----------------------------------------")

// const obj = {
//   name: "Shadab",
//   age: 24,
//   type: "human",
// };

// let result = Object.keys(obj).filter((ele) => ele !== "type");
// console.log(result);

// console.log(Object.keys(obj));
// console.log(Object.values(obj));
// console.log(Object.entries(obj));

// --------------------------------- 23rd May 3 Questions ----------------------------//

// arr = [1, 2, 3];
// result = arr.map((x, i) => {
//   return x + " " + arr[i + 1];
// });
// console.log(result);

// array = [1, 2, 3, 4, 5];
// //console.log(array.slice(1))
// console.log(
//   array.flatMap((ele, i) => array.slice(i + 1).map((ele2) => ele + " " + ele2))
// );
//[2,3,4,5,3,4,5,4,5,5]

// num = 1234;

// console.log(num.toString(2));

// newArr = [1, 2, 3];

// resultNew = newArr.reduce(
//   (a, cur) => a.concat(a.map((ele) => [cur].concat(ele))),
//   [[]]
// );
// console.log(resultNew);

// const every_nth = (arr, nth) =>
//   arr.filter((e, i) => {
//     if (i % nth === nth - 1) return e;
//   });
// //console.log(every_nth([1, 2, 3, 4, 5, 6], 1));
// console.log(every_nth([11, 232, 332, 24, 5, 646], 2));
// //                     0, 1, 2, 3, 4, 5
// //                     0, 1, 0, 1, 0, 1
// console.log(every_nth([1, 2, 3, 4, 5, 6], 3));
// // console.log(every_nth([1, 2, 3, 4, 5, 6], 4));

// let b = [[]];

// console.log([[]].concat([1,2,3]));

// arr = [1,2,3,'4','a'];

// console.log(arr.lastIndexOf(2));

// let str = 'SHADAB';

// const func1 = (str) => {
//   let strArr = [...str];
//   return (strArr[0].toLowerCase()+str.slice(1));

// }
// console.log(func1(str));

//--------------------- 26th May 2 Questions ----------
// Remove Duplicates from Array

// let arr = [1, 2, 3, 4, 4, 32, 99, 2];
// arr.sort((a,b) => a - b);
// console.log(arr);

// console.log([...new Set(arr)]);

// let temp = [];
// arr.forEach((ele) => {
//   if (!temp.includes(ele)) {
//     temp.push(ele);
//   }
// });
// console.log(temp);

// const obj1 = {
//   a: 1,
//   b: 23,
//   c: 2,
// };

// console.log(obj1);
// //remove 'c' key and its value
// // console.log(Object.keys(obj1));
// let temp = Object.keys(obj1).filter((ele, i) => ele !== "c");
// console.log(temp);

//let arr = [1, 2, 3, 4, 3, 4, 2, 5, 6];
// console.log(arr.length)
// for (let index = 0; index < arr.length; index++) {
//   console.log(index);
//   console.log(arr[index]);
//   console.log("---")
// }
// console.log("--------------------------")

// console.log(arr.indexOf(2));
// console.log(arr.lastIndexOf(2));

//const arr1 = arr.filter((ele) => arr.indexOf(ele) === arr.lastIndexOf(ele));
//console.log(arr1);

// let arr2 = [1, 2, 3, 4, 9];
// let arr3 = [1, 2, 3, 4, 5, 6, 7];

//let arrNew = arr2.concat(arr3) ;
//console.log(arrNew);
// console.log([...new Set(arr2.concat(arr3))]);

// num = 205468;
// let numArr = [...`${num}`].map((ele) => parseInt(ele));
// console.log(numArr);
// let result = [numArr[0]];
// console.log(result);
// for (let i = 1; i < numArr.length; i++) {
//   if (numArr[i - 1] % 2 === 0 && numArr[i] % 2 === 0) {
//     result.push("-", numArr[i]);
//   } else {
//     result.push(numArr[i]);
//   }
// }
// console.log(result.join(""));
// console.log(num);

// let arr2 = [1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4];

// let resultArr = arr2
//   .slice()
//   .sort(
//     (a, b) =>
//       arr2.filter((ele) => ele === a).length -
//       arr2.filter((ele) => ele === b).length
//   )
//   .pop();

// console.log(resultArr);

// num = 23;
// console.log(num.toString(2));

// //let arr = [1,2,3,'a','c',3,4,5]; //[1,2,'a','c',4];
// //         0 1 2  3   4  5 6 7

// console.log(arr.length);

// const result = (array, ...args) => {
//   console.log(array);
//   console.log(args);
//   let temp = array.filter((ele) => !args.includes(ele))
//   return temp;
// }

// console.log(result(arr,3,5));

// let arr = [1, 2, 3, 4, 3, 4, 2, 5, 6];

// // console.log([...new Set(arr)]);

// let temp = [];
// arr.forEach((ele) => {
//   if (!temp.includes(ele)) {
//     temp.push(ele);
//   }
// });

// console.log(temp);

// let result = arr.filter((ele) => arr.indexOf(ele) === arr.lastIndexOf(ele));
// console.log(result);

// let arr = ["a", 2, "b", 4, "c", "d"];
// let ogFltr = (array, args) => {
//   console.log(array);
//   console.log(args);
//   let tempArr = [];
//   let temp = array
//     .map((ele, i) => (args.includes(i) ? tempArr.push(ele) : ele))
//     .filter((ele, i) => !args.includes(i));
//   return temp;
// };

// console.log(ogFltr(arr, [2, 4]));

// let arr = [1, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4];

// let a = arr
//   .slice()
//   .sort(
//     (a, b) =>
//       arr.filter((ele) => ele === a).length -
//       arr.filter((ele) => ele === b).length
//   )
//   .pop();
// console.log(a);

////June 1st 3 Questions

// let text = "The Quick Brown Fox";

// let ans = text
//   .split("")
//   .map((ele) => (ele === ele.toUpperCase() ? ele.toLowerCase() : ele.toUpperCase()))
//   .join("");

// console.log(ans);

// let arr1 = [1,2,3];
// let arr2 = [100,2,1,10];

// console.log([...new Set(arr1.concat(arr2))]);

// function sumOfTwo(array, sumNumber) {
//   for (i of array) {
//     for (j of array) {
//         if (i + j === sumNumber) {
//             console.log([i, j])
//         }
//     }
//   }
// }
// sumOfTwo([1, 2, 3,4,5,6], 7)

////June 11th JS coding questions - 15 Questions

//1st Question
//Check wether a given input is Array or not
// let arr = [1, 2, 3, 4, 5];
// let str = "[1,2,3,4]";
// let num = 23;
// console.log(Array.isArray(num));

//2nd Question
//Return 1st n elements of array

// function first(arr, n) {
//   console.log(n);
//   if (arr == null) {
//     return void 0;
//   }
//   if (n == null) {
//     return arr[0];
//   }
//   if (n < 0) {
//     return [];
//   }
//   return arr.slice(0, n);
// }

// console.log(first([1, 2, 3, 4, 5]));

//3rd Question
//Return last n elements

// function last(arr, n) {
//   if (n == null) {
//     return arr[arr.length - 1];
//   }
//   if ((arr == null)) {
//     return void 0;
//   }
//   return arr.slice(Math.max(arr.length - n, 0));
// }

// console.log(last([7, 9, 0, -2]));
// console.log(last([7, 9, 0, -2],3));

// let arr = [2,3,4,5,6,7,8,9];
// console.log(arr.slice(0,1));
// console.log(arr.slice(4));

// console.log(arr.join('+'));

// let num = 205468;

// let arrNum = [...`${num}`].map((ele) => parseInt(ele));

// let result = [];
// result.push(arrNum[0]);

// for(let i = 1; i < arrNum.length; i++) {
//   if (arrNum[i-1] % 2 === 0 && arrNum[i] % 2 === 0) {
//     result.push('-',arrNum[i]);
//   }else{
//     result.push(arrNum[i])
//   }
// }

// console.log(result);

// let arrSrt = [5,11,1,2,3,-1,-2,-3,99];

// let res1 = arrSrt.sort((a,b) => a-b);
// console.log(res1);

// arr1 = [3, "a", "a", "a", 2, 3, "a", 3, "a", 2, 4, 9, 3];

// let res2 = arr1
//   .slice()
//   .sort(
//     (a, b) => arr1.filter((ele) => ele === a).length - arr1.filter((ele) => ele === b).length
//   )
//   .pop();

// console.log(res2);

// var str = 'The Quick Brown Fox';

// let arrStr = [...str];
// console.log(arrStr);
// let res = arrStr.map((ele) => (ele === ele.toUpperCase() ? ele.toLowerCase(): ele.toUpperCase()))
// console.log(res.join(''));

// console.log(str.split(''));

// const Prom  = new Promise((res,rej) => {
//   let roomClean = false;
//   if(roomClean) {
//     res("cleaned")
//   } else {
//     rej("not cleaned")
//   }
// })

// Prom.then((res) => {
//   console.log("The room is " + res);
// }).catch((rej) => {
//   console.log("The room is " + rej)
// })

// ------------------------- 19th June 2022 ---------------------------

//1st Day and 2nd Day

// var a = 4 + "3" + 2 + 8
// console.log(a);
// console.log(typeof(a));

// // let i = 0;
// // for(;i < 5 ;i++){}
// // console.log(i);

// let arr = [1,2,3,[2,3,4,[23,45],4,5,[[3,4,5,[2,3,4]]]]]
// console.log(arr.flat(Infinity));

// obj1 = {
//   a: 10
// }

// const obj2 = {...obj1 };
// obj2.a = 20;
// console.log(obj1);
// console.log(obj2);
// // let obj1;

// console.log(null === undefined);
// console.log(null == undefined);

// console.log(a,b,c);
// let a = 10;
// const b = 20;
// var c = 30;

// const nums = {
//   a: 20,
//   b: 40,
//   title: "my Nums"
//   }

//   console.log(JSON.stringify(nums));

// let arr = [1,2,3,4,5,2,3,5,6,7,8];

// let temp = [];
// arr.forEach((x) => {
//   if (!temp.includes(x)) {
//     temp.push(x)
//   }
// })

// arr.forEach((x) => {
//   x*2;
// })

// console.log(arr);
//console.log(temp);

// Array.prototype.myMap = function(cb){
//   let temp = [];
//   for (let i = 0 ; i < this.length; i++) {
//     temp.push(cb(this[i], i, this))
//   }
//   return temp;
// }

// const res = arr.myMap((ele) => {
//   return ele*2;
// })

// console.log(res);

// Array.prototype.myFilter = function(cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     if(cb(this[i])) temp.push(this[i])
//   }
//   return temp;
// }

// const res2 = arr.filter((ele) => {
//   return ele%2 === 0;
// })

// console.log(res2);

// let arr = [1,2,3,4,5];

// arr.forEach((ele,index) => {
//   arr[index] = ele*ele;
// });

// console.log(arr);

// let arr2 = [1,2,3,4,5];

// const arr3 = arr2.map((ele,index,array) => {
//   return ele*2
// })

// console.log(arr2);
// console.log(arr3);

// let arr = [1,2,3,4,5];

// Array.prototype.myMap = function(cb) {
// let temp = [];
// for(let i = 0; i < this.length; i++) {
// temp.push(cb(this[i],i,this))
// }
// return temp;
// }

// let res1 = arr.myMap((ele) => {
// return ele+1;
// })

// console.log(res1);

// Array.prototype.myFilter = function(cb) {
//   let temp = [];
//   for(let i = 0; i < this.length ; i++) {
//   if(cb(this[i])) temp.push(this[i])
//   }
//   return temp;
//   }

//   let res2 = arr.myFilter((ele) => {
//   if(ele%2 === 0) {
//   return ele;
//   }
//   })

//   console.log(res2);

// const obj = {
//   name: "Shadab",
//   age: 24
// }

// const info = obj;
// // info.name = "Aryan"
// // console.log(info===obj);
// // console.log(info==obj);

// console.log(obj);
// obj.age2 = 20;
// console.log(obj)

// const arr = [1,2,3];
// arr[0] = 10;
// arr.push(20);
// console.log(arr);

// const obj1 = {
//   a:10
// }

// const obj2 = JSON.parse(JSON.stringify(obj1));

// obj2.a = 20;

// console.log(obj1);
// console.log(obj2);

// const arr = [1,2,3];
// arr[0] = 10;
// arr[3] = 20;
// console.log(arr);
// arr.push(30);
// console.log(arr);

// console.log(1=='1');
// console.log(1==='1');

// console.log("------");

// console.log(false === 0);

// console.log("------");

// console.log(1>2>3);
// console.log(false>3);
// console.log(1<2<3);

// const array = [1, [2, 3],
//   [3, 4, 5], 6, [
//     [7, 8]
//   ]
// ];

// const flatten = (arr) => {
//   let res = arr.reduce((acc, item) => {
//     if (Array.isArray(item)) {
//       acc = acc.concat(flatten(item))
//     } else {
//       acc.push(item)
//     }
//     return acc;
//   }, [])
//   return res;
// }

// console.log(flatten(array))

// function add(x = 10, y = 20) {
//   console.log(x + y);
// }

// add(10, 30); // 40

// let arr = [1,2,3,4,5,6,7];

// console.log(arr.slice(2, 3))

// console.log(String("LCO") === "LCO");

// let name1 = {
//   name: "Shadab"
// }

// const person = [name1]

// name1 = null

// console.log(new String("Shadab") instanceof String);

// a = 10;
// b = 20;
// //console.log(a,b)
// [a,b] = [b,a]
// console.log(a,b)

// function find() {
//   let a = [];
//   for (let i = 0; i < 1000000; i++) {
//     a[i] = i * i;
//   }
//   return function (index) {
//     console.log(a[index]);
//   };
// }

// let closure = find();
// console.log(closure);
// console.time("6");
// closure(6);
// console.timeEnd("6");

// function a() {
//   for (var i = 0; i < 3; i++) {
//     (function (i) {
//       setTimeout(function log() {
//         console.log(i);
//       }, i * 1000);
//     })(i);
//   }
// }
// a();

// function f() {
//   console.log(this);
// }

// let user = {
//   g: f.bind(null),
// };

// user.g();

// function f() {
//   console.log(this.name)
// }

// f = f.bind({name: "Shadab"}).bind({name: "Hussain"})
// f()

// const makeApiCall = (time) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res("Api Call in " + time);
//     }, time);
//   });
// };

// let multipleApiCall = [makeApiCall(1000), makeApiCall(500), makeApiCall(2000)];

// Promise.all(multipleApiCall).then((val) => console.log(val));
// Promise.race(multipleApiCall).then((val) => console.log(val));

// const promise = new Promise((resolve, reject) => {
//   if (false) {
//     resolve("The promise is resolved");
//   } else {
//     reject("The promise is rejected");
//   }
// });

// promise
//   .then((val) => console.log(val))
//   .catch((val) => console.log(val))
//   .finally(() => console.log("Clean up Activity"));

// const p = new Promise((res, rej) => {
//   isClean = false;
//   if (isClean) {
//     res("Room is cleaned");
//   } else {
//     rej("Room is not cleaned");
//   }
// });

// p.then((val) => console.log(val))
//   .catch((val) => console.log(val))
//   .finally(() => console.log("CleanUp"));

// const apiCall = (time) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res("Api call in " + time);
//     }, time);
//   });
// };

// const multipleApiCall = [apiCall(1000), apiCall(2000), apiCall(500)];

// Promise.all(multipleApiCall).then((val) => console.log(val));
// Promise.race(multipleApiCall).then((val) => console.log(val));

// console.log("Start");

// const impMsg = (username) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res(`Subscribe to ${username}`);
//     }, 1000);
//   });
// };

// const impAction = (username) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res(`Like to ${username} video`);
//     }, 1000);
//   });
// };

// const impAction2 = (username) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res(`Share the ${username} video`);
//     }, 1000);
//   });
// };

// impMsg("RS coder").then((val) => console.log(val));
// impAction("RS coder").then((val) => console.log(val));
// impAction2("RS coder")
//   .then((val) => console.log(val))
//   .catch((val) => console.log(val));

// console.log("Stop");

// console.log("-----")
// console.log("Start2")

// const Message = (username) => {
//   setTimeout(() => {
//     return `Sunbscribe to ${username}`
//   },1000)
// }

// const MSG = Message("RS coder")

// console.log(MSG);

// console.log("Stop")

// const apiCall = (time) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res(`The Api call executed in ${time}`);
//     }, time);
//   });
// };

// const multipleApiCall = [apiCall(1000), apiCall(2000), apiCall(500)];

//Promise.all(multipleApiCall).then((val) => console.log(val));
//Promise.race(multipleApiCall).then((val) => console.log(val));
// Promise.allSettled(multipleApiCall).then((val) => console.log(val));
//Promise.any(multipleApiCall).then((val) => console.log(val));

// const impMsg = (username) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res(`Subscribe to ${username}`);
//     }, 2000);
//   });
// };

// const impAction = (username) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       res(`Like the ${username} video`);
//     }, 1000);
//   });
// };

// const impAction2 = (username) => {
//   return new Promise((res, rej) => {
//     setTimeout(() => {
//       rej(`share the ${username} video`);
//     }, 500);
//   });
// };

// Promise.any([impMsg("RS coder"), impAction("RS coder"), impAction2("RS coder")])
//   .then((val) => console.log(val))
//   .catch((err) => console.log("the error is " + err));

// console.log("Start");

// const promise1 = new Promise((resolve, reject) => {
//   console.log(1);
//   resolve(2);
//   console.log(3);
// });

// promise1.then((val) => console.log(val));

// console.log("Stop");

// let arr = [1, 2, 3, 4, 5];

// Array.prototype.myEach = function (cb) {
//   for (let i = 0; i < this.length; i++) {
//     cb(this[i], i, this);
//   }
// };

// arr.myEach((x, i) => {
//   arr[i] = x * x;
// });

// console.log(arr);

// Array.prototype.myMap = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     temp.push(cb(this[i], i, this));
//   }
//   return temp;
// };

// let res1 = arr.myMap((x) => {
//   return x * 2;
// });
// console.log(res1);

// Array.prototype.myFilter = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     if (cb(this[i])) temp.push(this[i]);
//   }
//   return temp;
// };

// let res2 = arr.myFilter((x) => {
//   return x % 2 === 0;
// });
// console.log(res2);

// Array.prototype.myReduce = function (cb, initialValue) {
//   let acc = initialValue;
//   for (let i = 0; i < this.length; i++) {
//     acc = acc ? cb(acc, this[i], i, this) : this[i];
//   }
//   return acc;
// };

// let res3 = arr.myReduce((acc, item) => {
//   return acc + item;
// }, 0);
// console.log(res3);

// const obj1 = {
//   name: "Shadab",
//   age: 25,
// };

// function myInfo(city) {
//   console.log(`My name is ${this.name} and age is ${this.age} from ${city}`);
// }

// // myInfo.bind(obj1)("Hyd");

// Function.prototype.myCall = function (context = {}, ...args) {
//   if (typeof this !== "function") {
//     throw new Error(this + "It is not callable");
//   }

//   context.fn = this;
//   context.fn(...args);
// };
// //myInfo.myCall(obj1, "Hyd");

// Function.prototype.myApply = function (context = {}, args = []) {
//   if (typeof this !== "function") {
//     throw new Error(this + "is not Callable");
//   }

//   if (!Array.isArray(args)) {
//     throw new Error("Args is not an Array");
//   }

//   context.fn = this;
//   context.fn(...args);
// };
// //myInfo.myApply(obj1, ["Hyd"]);

// Function.prototype.myBind = function (context = {}, ...args) {
//   if (typeof this !== "function") {
//     throw new Error(this + "Cannot bind as it is not callble");
//   }
//   context.fn = this;
//   return function (...newArgs) {
//     return context.fn(...args, newArgs);
//   };
// };
// myInfo.myBind(obj1)("Hyd")

// const obj1 = new Object();

// console.log(obj1);

// const obj2 = Object.create(null);

// console.log(obj2);

// const obj3 = Object.assign({}, undefined);

// console.log(obj3);

// function Person(name, age) {
//   this.name = name;
//   this.age = age;
// }

// const obj4 = new Person("Shadab", 24);

// console.log(obj4);

// const Person1 = (name, age) => {
//   this.name = name;
//   this.age = age;
// };

// // const obj5 = new Person1("ShadabHussain", 25);
// // console.log(obj5);

// console.log(Person.prototype);
// console.log(Person1.prototype);

// const obj6 = {
//   name: "Shadab",
//   myInfo: function () {
//     name: "Hussain"
//     console.log(this.name);
//   },
// };

// obj6.myInfo();

// Array.prototype.myEach = function (cb) {
//   for (let i = 0; i < this.length; i++) {
//     cb(this[i], i, this);
//   }
// };

// let arr = [1, 2, 3, 4];

// // arr.myEach((ele, i) => {
// //   arr[i] = ele * ele;
// // });

// // console.log(arr);

// Array.prototype.myMap = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     temp.push(cb(this[i], i, this));
//   }
//   return temp;
// };

// const res = arr.myMap((ele) => {
//   return ele * ele;
// });

// console.log(res);

// Array.prototype.myFilter = function (cb) {
//   let temp = [];
//   for (i = 0; i < this.length; i++) {
//     if (cb(this[i], i, this)) temp.push(this[i]);
//   }
//   return temp;
// };

// const res2 = arr.myFilter((ele) => {
//   return ele % 2 === 0;
// });
// console.log(res2);

// Array.prototype.myReduce = function (cb, initialValue) {
//   let acc = initialValue;
//   for (let i = 0; i < this.length; i++) {
//     acc = acc ? cb(acc, this[i], i, this) : this[i];
//   }
//   return acc;
// };

// const res3 = arr.myReduce((acc, cur) => {
//   return acc + cur;
// });
// console.log(res3);

// const user = {
//   name: "Shadab",
//   getInfo: function () {
//     console.log(this.name);
//   },
// };

// setTimeout(() => {
//   user.getInfo();
// }, 1000);

// const calc = {
//   total: 0,
//   add(n) {
//     this.total += n;
//     return this;
//   },
//   sub(n) {
//     this.total -= n;
//     return this;
//   },
//   mul(n) {
//     this.total *= n;
//     return this;
//   },
//   div(n) {
//     this.total /= n;
//     return this;
//   },
// };

// const result = calc.add(10).mul(5).sub(30).add(10).div(2);
// console.log(result.total);

// const obj1 = {
//   name: "Shadab",
//   age: 25,
// };

// function myInfo(city) {
//   console.log(`My name is ${this.name} and my age is ${this.age} from ${city}`);
// }

// // // myInfo.call(obj1, "Hyd");
// // myInfo.apply(obj1, ["Mum"]);
// // myInfo.bind(obj1)("Del");

// Function.prototype.myCall = function (context = {}, ...args) {
//   if (typeof this !== "function") {
//     throw new Error(this + "is not callable");
//   }
//   context.fn = this;
//   context.fn(...args);
// };

// myInfo.myCall(obj1, "Hyd");

// Function.prototype.myApply = function (context = {}, args = []) {
//   if (typeof this !== "function") {
//     throw new Error(this + "is not callable");
//   }

//   if (!Array.isArray(args)) {
//     throw new Error("error");
//   }

//   context.fn = this;
//   context.fn(...args);
// };

// myInfo.myApply(obj1, ["Mum"]);

// Function.prototype.myBind = function (context = {}, ...args) {
//   if (typeof this !== "function") {
//     throw new Error(this + "cannot bound as it is not callable");
//   }

//   context.fn = this;
//   return function (...newArgs) {
//     return context.fn(...args, ...newArgs);
//   };
// };
// myInfo.myBind(obj1)("Del");

// let newArr = [1, 2, 3, [4, 5, [6, 7], 8], 9, [10]];

// const flatten = (arr) => {
//   const res = arr.reduce((acc, item) => {
//     if (Array.isArray(item)) {
//       acc = acc.concat(flatten(item));
//     } else {
//       acc = [...acc, item];
//     }
//     return acc;
//   }, []);
//   return res;
// };

// console.log(flatten(newArr));

// const sum = (a) => (b) => b ? sum(a + b) : a;
// console.log(sum(1)(2)(3)(4)());

// const btn = document.querySelector(".increment_btn");
// const btnClicked = document.querySelector(".btn_clicked");
// const btnTriggered = document.querySelector(".btn_triggered");

// const myDebounce = (cb, delayTime) => {
//   let timer;
//   return function (...args) {
//     if (timer) clearTimeout(timer);
//     timer = setTimeout(() => {
//       cb(...args);
//     }, delayTime);
//   };
// };

// const myDebounce = (cb, delayTime) => {
//   let timer;
//   return function (...args) {
//     if (timer) clearTimeout(timer);
//     timer = setTimeout(() => {
//       cb(...args);
//     }, delayTime);
//   };
// };

// const myThrottle = (cb, delayTime) => {
//   let last = 0;
//   return function (...args) {
//     let now = new Date().getTime();
//     if (now - last < delayTime) return;
//     last = now;
//     cb(...args);
//   };
// };
// const myThrottle = (cb, delayTime) => {
//   let last = 0;
//   return function (...args) {
//     let now = new Date().getTime();
//     if (now - last < delayTime) return;
//     last = now;
//     return cb(...args);
//   };
// };

// var btnClickedCount = 0;
// var btnTriggeredCount = 0;

// const debounce = myThrottle(() => {
//   btnTriggered.innerHTML = ++btnTriggeredCount;
// }, 800);

// btn.addEventListener("click", () => {
//   btnClicked.innerHTML = ++btnClickedCount;
//   debounce();
// });

// function createBase(n) {
//   return function (m) {
//     console.log(n + m);
//   };
// }

// var addSix = createBase(6);
// addSix(10); //return 16
// addSix(21); //return 27

// function res() {
//   let a = [];
//   for (let i = 0; i < 100000; i++) {
//     a[i] = i * i;
//   }
//   return function (index) {
//     console.log(a[index]);
//   };
// }

// const find = res();
// console.time("6");
// find(6);
// console.timeEnd("6");

// const obj1 = {
//   a: "Shadab",
//   b: 24,
//   c: "Hussain",
//   d: {
//     name: "Deloitte",
//   },
// };

// const {
//   d: { name: company },
// } = obj1;

// const { c: surname } = obj1;

// console.log(surname);
// console.log(company);

// let a;
// let b;
// let rest;

// [a, b, ...rest] = [10, 20, 30, 40, 50];

// console.log(a);
// console.log(b);
// console.log(rest);

// let gp = document.getElementById("gp");
// let p = document.getElementById("p");
// let child = document.getElementById("child");

// gp.addEventListener("click", () => {
//   console.log("Grand Parent Clicked");
// });

// p.addEventListener("click", (e) => {
//   console.log("Parent Clicked");
//   e.stopImmediatePropagation();
// });

// child.addEventListener("click", () => {
//   console.log("Child Clicked");
// });

let arr = [1, 2, 3, 4, 5, 6, 7, 8];

// console.log(arr.slice(3, 6));

console.log(arr.splice(3, 1));
console.log(arr);
