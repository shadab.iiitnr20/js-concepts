// console.log("Hello World");

// console.log(a);
// console.log(b);
// console.log(c);

// function call() {
//   console.log(a);
//   console.log(b);
//   console.log(c);
// }

// var a = 10;
// let b = 20;
// const c = 30;

// call();

// const obj1 = {
//   a: 10,
// };

// const obj2 = { ...obj1 };

// obj2.a = 20;

// console.log(obj1);

// const obj1 = {
//   name: "Shadab",
//   myInfo: function () {
//     console.log(this);
//     name: "Hussain";
//     console.log(this.name);
//   },
//   myInfo2: function () {
//     const myInfo3 = () => console.log(this.name);
//     myInfo3();
//   },
// };

// obj1.myInfo();
// obj1.myInfo2();

// const calc = {
//   read() {
//     this.a = +prompt("a = ", 0);
//     this.b = +prompt("b = ", 0);
//   },
//   sum() {
//     return this.a + this.b;
//   },
//   mul() {
//     return this.a * this.b;
//   },
// };

// calc.read();
// console.log(calc.sum());
// console.log(calc.mul());

// const calc = {
//   total: 0,
//   add(n) {
//     this.total += n;
//     return this;
//   },
//   sub(n) {
//     this.total -= n;
//     return this;
//   },
//   mul(n) {
//     this.total *= n;
//     return this;
//   },
//   div(n) {
//     this.total /= n;
//     return this;
//   },
// };

// const res = calc.add(10).mul(5).sub(30).add(10);
// console.log(res.total);

// console.log(3 > 2 > 1);
// console.log(1 < 2 < 3);

// const sum = (a) => (b) => b ? sum(a + b) : a;

// console.log(sum(2)(3)(4)());

// const arr = [2, 3, 4, 5];

// // const res = arr.forEach((ele, i) => {
// //   arr[i] = ele * ele;
// // });

// // console.log(res);

// // const res2 = arr.map((ele, i) => {
// //   return ele * ele;
// // });

// // console.log(arr);
// // console.log(res2);

// Array.prototype.myEach = function (cb) {
//   for (let i = 0; i < this.length; i++) {
//     cb(this[i], i, this);
//   }
// };

// arr.myEach((ele, i) => {
//   arr[i] = ele + 1;
// });

// console.log(arr);

// Array.prototype.myMap = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     temp.push(cb(this[i], i, this));
//   }
//   return temp;
// };

// const res = arr.myMap((ele) => {
//   return ele * 2;
// });
// console.log(arr);
// console.log(res);

// Array.prototype.myFilter = function (cb) {
//   let temp = [];
//   for (let i = 0; i < this.length; i++) {
//     if (cb(this[i])) temp.push(this[i]);
//   }
//   return temp;
// };

// const res2 = arr.myFilter((ele) => {
//   return ele > 4;
// });
// console.log(res2);

// Array.prototype.myReduce = function (cb, initialValue) {
//   let acc = initialValue;
//   for (let i = 0; i < this.length; i++) {
//     acc = acc ? cb(acc, this[i], i, this) : this[i];
//   }
//   return acc;
// };

// const res3 = arr.myReduce((e, i) => {
//   return e + i;
// }, 0);

// console.log(res3);

// const obj2 = {
//   name: "Shadab",
//   age: 25,
// };

// console.log(obj2["name"]);

// const items = document.getElementById("items");
// items.addEventListener("click", (e) => {
//   console.log(e.target.id);
//   window.location.href = "/" + e.target.id;
// });

// const btn = document.getElementById("btn");
// const click = document.getElementById("click");
// const trg = document.getElementById("trg");

// var count_click = 0;
// var count_trg = 0;

// const myDebounce = (cb, d) => {
//   let timer;
//   return function (...args) {
//     if (timer) clearTimeout(timer);
//     timer = setTimeout(() => {
//       cb(...args);
//     }, d);
//   };
// };

// const debounce = myDebounce(() => {
//   trg.innerHTML = ++count_trg;
// }, 800);

// const myThrottle = (cb, d) => {
//   let last = 0;
//   return function (...args) {
//     let now = new Date().getTime();
//     if (now - last < d) return;
//     last = now;
//     return cb(...args);
//   };
// };

// const throttle = myThrottle(() => {
//   trg.innerHTML = ++count_trg;
// }, 800);

// btn.addEventListener("click", () => {
//   click.innerHTML = ++count_click;
//   throttle();
// });

// const array = [1, 2, 3, [4, 5, [6, 7], 8, 9], 10];

// const flatten = (arr) => {
//   let res = arr.reduce((acc, item) => {
//     if (Array.isArray(item)) {
//       acc = acc.concat(flatten(item));
//     } else {
//       acc.push(item);
//     }
//     return acc;
//   }, []);
//   return res;
// };

// console.log(flatten(array));

// const gp = document.getElementById("gp");
// const p = document.getElementById("p");
// const child = document.getElementById("child");

// gp.addEventListener("click", () => {
//   console.log("grandparent clicked");
// });

// p.addEventListener("click", () => {
//   console.log("parent clicked");
// });

// child.addEventListener("click", (e) => {
//   console.log("child clicked");
//   e.stopPropagation();
// });

// const btn = document.getElementById("btn");
// const clicked = document.getElementById("click");
// const triggered = document.getElementById("trg");

// var btn_clicked = 0;
// var btn_triggered = 0;ss

// // const myDebounce = function (cb, delayTime) {
// //   let timer;
// //   return function (...args) {
// //     if (timer) clearTimeout(timer);
// //     timer = setTimeout(() => {
// //       cb(...args);
// //     }, delayTime);
// //   };
// // };

// const myThrottle = (cb, delayTime) => {
//   let last = 0;
//   return function (...args) {
//     let now = new Date().getTime();
//     if (now - last < delayTime) return;
//     last = now;
//     return cb(...args);
//   };
// };

// const throttle = myThrottle(() => {
//   triggered.innerHTML = ++btn_triggered;
// }, 1000);

// btn.addEventListener("click", () => {
//   clicked.innerHTML = ++btn_clicked;
//   throttle();
// });

// const arr2 = [1, 2, 3, [4, 5, [6, 7], 8, [9]]];

// const flatten = (arr) => {
//   const res = arr.reduce((acc, item) => {
//     if (Array.isArray(item)) {
//       acc = acc.concat(flatten(item));
//     } else {
//       acc = [...acc, item];
//     }
//     return acc;
//   }, []);
//   return res;
// };

// console.log(flatten(arr2));

// const obj = {
//   name: "Shadab",
//   age: 25,
// };

// function myInfo(city) {
//   console.log(`My Name is ${this.name} and age ${this.age} from ${city}`);
// }

// Function.prototype.myCall = function (context = {}, ...args) {
//   if (typeof this !== "function") {
//     throw new Error("The function is not callable");
//   }
//   context.fn = this;
//   context.fn(...args);
// };

// Function.prototype.myApply = function (context = {}, args = []) {
//   if (typeof this !== "function") {
//     throw new Error(this + "it is not callable");
//   }
//   if (Array.isArray(args)) {
//     throw new TypeError("It is not an Array");
//   }
//   context.fn = this;
//   context.fn(...args);
// };

// Function.prototype.myBind = function (context = {}, ...args) {
//   if (typeof this !== "function") {
//     throw new Error(this + "it is not callable");
//   }
//   context.fn = this;
//   return function (...newArgs) {
//     return context.fn(...args, ...newArgs);
//   };
// };

// myInfo.myCall(obj, "Hyd");
// myInfo.myApply(obj, ["Hyd"]);
// const myInfoLater = myInfo.bind(obj);
// myInfoLater("Hyd");

const arr = [2, 4, 4, 26, 3, 2, 6, 7, 8, 9, 1, 10, 12, 11];

const arr2 = [...new Set(arr)];

console.log(arr2);

const res = arr2.sort((a, b) => a - b);
console.log(res);

function Bsearch(res, n) {
  if(res.length === 1 && res[0] != n) return false
  let mid = Math.floor(res.length/2)
  if(res[mid]==n) return mid;
  else if (n > res[mid]) return Bsearch(res.slice(mid), n);
  else if(n < res[mid]) return Bsearch(res.slice(0, mid), n)

}


console.log(Bsearch(res, 4));

// const arr5 = [12,3,4];

// const res2 = arr5.reduce((a,b) => a+b);
// console.log(res2);
